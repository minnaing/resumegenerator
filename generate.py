
# -*- coding: utf-8 -*-

#from jinja2 import Environment, PackageLoader
#env = Environment(loader=PackageLoader('yourapplication', 'templates'))

#template = env.get_template('theme.html')

#print template.render(the='variables', go='here')

from jinja2 import Template
import json
import codecs

def read_file(fileName):
	try:
		f = codecs.open(fileName, 'rb', encoding='utf-8')
		result = f.read()
		#print " - File:", fileName, " read."
		f.close()
		return result
	except IOError:
		print " ERROR: File:", fileName, " Not Found"


def load_json(jsonFile):
	return json.loads(read_file(jsonFile))


def write_file(text, outputFile):
	try:
		f = codecs.open(outputFile, "wb", "utf-8")
		f.write(text)
		f.close()
		#print " - File:", outputFile, " generated."
	except:
		print " ERROR: Can't write", outputFile, "."

def generate(themeFile, jsonFile, outputFile):
	try:
		template = Template(read_file(themeFile))
		print " SUCCESS: File:%s loaded as Template." % themeFile
		data = load_json(jsonFile)
		print " SUCCESS: File:%s loaded as Data." % jsonFile
		content = template.render(data)
		print " SUCCESS: Content generated."
		write_file(content, outputFile)	
		print " SUCCESS: File:%s generated as Output." % outputFile
	except Exception, e:
		print e
		print "Make sure filenames are correct."

HTMLthemeFile = './templates/bootstrap.html'
TXTthemeFile = './templates/sample.txt'
jsonFile = 'data.json'
HTMLoutputFile = "./out/test.html"
TXToutputFile = "./out/test.txt"


generate(HTMLthemeFile, jsonFile, HTMLoutputFile)
print 
generate(TXTthemeFile, jsonFile, TXToutputFile)
print 
#generate('./templates/paper.html', jsonFile, "./out/paper.html")

#generate('./templates/sample.tex', jsonFile, './out/sample.tex')


